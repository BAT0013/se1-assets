const STYLE_PATH = "https://gitlab.rrz.uni-hamburg.de/BAT0013/se1-assets/-/raw/main/style/";

function injectStyle(stylesheet) {
    let link = document.createElement("link");
    link.href = STYLE_PATH + stylesheet;
    link.type = "text/css";
    link.rel = "stylesheet";
    document.head.appendChild(link);
}

document.addEventListener("DOMContentLoaded", function() {
    injectStyle("assignment.css");
})